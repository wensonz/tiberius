(function () {
    
    var module = null;
    
    /**
     * AppController is the root controller for this app
     * 
     * @class AppController
     * @constructor
     * @param {Scope} $scope the scope object for this controller
     */
    function AppController($scope, alertService) {
        
        var index = 0,
            types = ['success','info', 'warning', 'danger'];
        
        $scope.titlebar = {
            title: 'Dashboard',
            icon: 'home',
            description: 'Welcome to ROME Dashboard'
        };
        
        $scope.addAlert = function () {
            var type = types[Math.round(Math.random() * 4) % 4];
            alertService.addAlert(type, type.toUpperCase() + '! ', 'Randomized alert generated at ' + Date.now());
        };
        
    }
    
    
    module = angular.module('tiberius.app', ['ui.bootstrap', 'tiberius.ui.alert']);
    module.controller(
        'AppController', ['$scope', 'alertService', AppController]
    );
    
})();
