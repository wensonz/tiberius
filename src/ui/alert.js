(function () {
    
    var module = null;
    
    /**
     * AlertService is designed to manage the alerts from controllers
     *
     * @class AlertService
     * @constructor
     */
    function AlertService($interval) {
        this.nextId_ = 0;
        this.index_ = {};
        this.alerts = [];
        
        this.timer_ = null;
        this.interval_ = $interval;
        this.lifecycle_ = 20000; // display 5000 by default
        this.tick_ = 1000;
    }
    
    /**
     * Add an alert
     *
     * @method addAlert
     * @param {String} type one of the following four types:
     *                      success, info, warning and danger
     * @param {String} title the title of this alert, which is to be displayed
     *                       in bold by default implementation
     * @param {String} message the message of this alert 
     */
    AlertService.prototype.addAlert = function addAlert(type, title, message) {
        var alert = {
            // id: this.nextId_,
            type: type,
            title: title,
            message: message,
            timestamp: Date.now()
        };
        
        this.alerts.push(alert);
        // this.index_[this.nextId_] = this.alerts.length - 1;
        // this.nextId_ += 1;
        if (!this.timer_) {
            this.timer_ = this.interval_(this.onTimer_.bind(this), this.tick_);
        }
    };
    
    /**
     * Remove the alert specified by its id
     *
     * @method removeAlert
     * @param {Number} id the id of the alert to be removed
     */
    AlertService.prototype.removeAlert = function removeAlert(index) {
        var // index = this.index_[id],
            alert = null;
        
        if (typeof index !== 'number') {
            return;
        }
        
        alert = this.alerts[index];
        /*
        if (alert.id !== id) {
            return;
        }
        */
        // delete this.index_[id];
        this.alerts.splice(index, 1);
        if (0 === this.alerts.length) {
            this.interval_.cancel(this.timer_);
            this.timer_ = null;
        }
    };
    
    /**
     * Event handler for interval to remove the end-of-life alerts
     *
     * @method onTimer_
     */
    AlertService.prototype.onTimer_ = function onTimer_() {
        var alert = null;
        
        while (this.alerts.length) {
            alert = this.alerts[0];
            if (alert.timestamp + this.lifecycle_ > Date.now()) {
                break;
            }
            
            delete this.index_[alert.id];
            this.alerts.shift();
        }
        
        if (0 === this.alerts.length) {
            this.interval_.cancel(this.timer_);
            this.timer_ = null;
        }
    };
    
    
    /**
     * Controller for the alert view
     *
     * @class AlertViewController
     * @constructor
     * @param {Scope} $scope the scope object for this controller
     * @param {AlertService} alertService the alert service instance
     */
    function AlertViewController($scope, alertService) {
        $scope.alerts = alertService.alerts;
        $scope.removeAlert = alertService.removeAlert.bind(alertService);
    }
    
    module = angular.module('tiberius.ui.alert', []);
    module.service('alertService', ['$interval', AlertService]);
    module.controller('AlertViewController', ['$scope', 'alertService', AlertViewController]);
})();
