#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import os

import bottle

static = os.path.normpath(os.path.join(os.path.dirname(__file__), '../src'))

@bottle.route('/')
def index():
    return bottle.static_file('app/tiberius.html', static)

exts = [
    '.html', '.js', '.css', '.ico', '.jpg', '.png', '.ttf', '.woff', '.svg', 
    '.eot', '.otf'
]

@bottle.route('/<path:path>')
def default(path):
    path = path.split('?')[0].split('#')[0]
    _, ext = os.path.splitext(path)
    if ext in exts:
        return bottle.static_file(path, static)


remote = 'http://10.210.214.234:8080'

bottle.run(port=8081, debug=True)
